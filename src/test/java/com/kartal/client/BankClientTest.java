package com.kartal.client;

import com.google.common.util.concurrent.Uninterruptibles;
import com.kartal.models.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.sound.midi.Soundbank;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

// to avoid static methods
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BankClientTest {

    // this is blocking stub so it is SYNC, response is waiting for request.
    private BankServiceGrpc.BankServiceBlockingStub blockingStub;
    // non blocking stub, it is ASYNC
    private BankServiceGrpc.BankServiceStub bankServiceStub;

    @BeforeAll
    public void setup() {
        // created channel
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();
        // created service
        blockingStub = BankServiceGrpc.newBlockingStub(managedChannel);
        bankServiceStub = BankServiceGrpc.newStub(managedChannel);
    }

    @Test
    public void balanceTest() {
        Balance balance = blockingStub.getBalance(BalanceCheckRequest.newBuilder()
                .setAccountNumber(1)
                .build());
        System.out.println(
                "Received : " + balance.getAmount()
        );
    }

    @Test
    public void withDrawTest() {
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(6)
                .setAmount(20)
                .build();
        blockingStub.withDraw(withDrawRequest)
                .forEachRemaining(
                        money -> System.out.println("Received:" + money.getValue())
                );
    }

    @Test
    public void withDrawAsyncTest() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        WithDrawRequest withDrawRequest = WithDrawRequest.newBuilder()
                .setAccountNumber(10)
                .setAmount(20)
                .build();
        bankServiceStub.withDraw(withDrawRequest, new MoneyStreamingResponse(latch));
        latch.await();
        // used to get async response, do this or CountDownLatch
        // Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    @Test
    public void cashStreamingRequest() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        // ASYNC
        StreamObserver<DepositRequest> streamObserver =
                this.bankServiceStub.cashDeposit(new BalanceStreamObserver(latch));
        for (int i = 0; i < 10; i++) {
            DepositRequest depositRequest = DepositRequest.newBuilder().setAccountNumber(1).setAmount(10).build();
            streamObserver.onNext(depositRequest);
            System.out.println("Client Request " + (i+1));
        }
        streamObserver.onCompleted();
        latch.await();
    }

}
