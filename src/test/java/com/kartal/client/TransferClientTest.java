package com.kartal.client;

import com.kartal.models.*;
import com.kartal.server.TransferService;
import com.kartal.server.TransferStreamimgRequest;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;

// to avoid static methods
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TransferClientTest {

    // non blocking stub, it is ASYNC
    private TransferServiceGrpc.TransferServiceStub transferServiceStub;

    @BeforeAll
    public void setup() {
        // created channel
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("localhost", 6565)
                .usePlaintext()
                .build();
        // created service
        transferServiceStub = TransferServiceGrpc.newStub(managedChannel);
    }

    @Test
    public void transferStreamingRequest() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        // ASYNC
        TransferStreamingResponse transferStreamingResponse = new TransferStreamingResponse(latch);
        StreamObserver<TransferRequest> transferRequestStreamObserver = this.transferServiceStub.transfer(transferStreamingResponse);
        for (int i = 0; i < 100; i++) {
            TransferRequest transferRequest = TransferRequest.newBuilder()
                    .setFromAccount(ThreadLocalRandom.current().nextInt(1,11))
                    .setToAccount(ThreadLocalRandom.current().nextInt(1,11))
                    .setAmount(ThreadLocalRandom.current().nextInt(1,21))
                    .build();

            transferRequestStreamObserver.onNext(transferRequest);
        }
        transferRequestStreamObserver.onCompleted();
        latch.await();
    }

}
