package com.kartal.server;

import com.kartal.models.TransferRequest;
import com.kartal.models.TransferResponse;
import com.kartal.models.TransferServiceGrpc;
import io.grpc.stub.StreamObserver;

public class TransferService extends TransferServiceGrpc.TransferServiceImplBase {
    @Override
    public StreamObserver<TransferRequest> transfer(StreamObserver<TransferResponse> responseObserver) {
        return new TransferStreamimgRequest(responseObserver);
    }
}
