package com.kartal.server;

import com.kartal.models.Account;
import com.kartal.models.TransferRequest;
import com.kartal.models.TransferResponse;
import com.kartal.models.TransferStatus;
import io.grpc.stub.StreamObserver;

public class TransferStreamimgRequest implements StreamObserver<TransferRequest> {

    public TransferStreamimgRequest(StreamObserver<TransferResponse> transferResponseStreamObserver) {
        this.transferResponseStreamObserver = transferResponseStreamObserver;
    }

    private StreamObserver<TransferResponse> transferResponseStreamObserver;

    @Override
    public void onNext(TransferRequest transferRequest) {
        int fromAccount = transferRequest.getFromAccount();
        int toAccount = transferRequest.getToAccount();
        int amount = transferRequest.getAmount();
        int balance = AccountDatabase.getBalance(fromAccount);
        TransferStatus transferStatus = TransferStatus.FAILED;

        int newFromAccount = 0, newToAccount = 0;
        if(balance <= amount || fromAccount != toAccount){
            transferStatus = TransferStatus.SUCCESS;
            newFromAccount = AccountDatabase.deductBalance(fromAccount, amount);
            newToAccount = AccountDatabase.addBalance(toAccount, amount);
        }
        Account fromAccount1 = Account.newBuilder()
                .setAccountNumber(fromAccount)
                .setAmount(newFromAccount)
                .build();

        Account toAccount1 = Account.newBuilder()
                .setAccountNumber(toAccount)
                .setAmount(newToAccount)
                .build();

        TransferResponse transferResponse = TransferResponse.newBuilder()
                .setStatus(transferStatus)
                .addAccount(fromAccount1)
                .addAccount(toAccount1)
                .build();
        transferResponseStreamObserver.onNext(transferResponse);
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void onCompleted() {
        AccountDatabase.printAll();
        transferResponseStreamObserver.onCompleted();
    }
}
