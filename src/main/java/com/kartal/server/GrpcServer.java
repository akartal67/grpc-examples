package com.kartal.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

// expose our service to outside
public class GrpcServer {

    public static void main(String[] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(6565)
                .addService(new BankService())
                .addService(new TransferService())
                .build();

        System.out.println("server started...");
        server.start();
        // to keep live
        server.awaitTermination();


    }

}
